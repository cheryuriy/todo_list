# ToDo List of organizations

Django 1.11+\
Django Rest Framework 3+

## You will need:

Python 3.5+\
And virtualenv activated

## Installation of dependencies:

``pip install -r requirements.txt``

## Run server:
 ``./manage.py runserver``

## Additionally:
For faster access there is test database db.sqlite3\
If you don't want to use it, delete it, and do:\
 ``./manage.py migrate``\
 Passwords and emails in test db are the same:\
 admin:\
`ad@mail.ru`

workers:\
`ok@mail.ru`\
`yl@mail.ru`\
`an@mail.ru`