from .models import *
from rest_framework import serializers
from rest_framework.reverse import reverse
from django.shortcuts import redirect


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('password', 'email')


class ProfileSerializer(serializers.ModelSerializer):
    """These are 2 serializers for User and it's Profile."""

    user = UserSerializer()

    class Meta:
        model = Profile
        fields = ('user', 'first_name', 'last_name', 'organizations')

    def create(self, validated_data):
        """Creation of User and it's Profile."""

        user_data = validated_data.pop('user')
        user_data['is_staff'] = False
        user_data['is_superuser'] = False
        user = User.objects.create_user(**user_data)
        organizations = validated_data.pop('organizations')
        profile = Profile.objects.create(user=user, **validated_data)
        for organization in organizations:
            profile.organizations.add(organization)
        return profile


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'password'})

    class Meta:
        model = Profile
        fields = ('email', 'password', 'active_company')

    def update(self, instance, validated_data):
        """
        Update Profile by active_company.
        """

        active_company = validated_data.get('active_company')
        company = instance.organizations.filter(name=active_company).first()
        if company:
            instance.active_company = company
            instance.save(update_fields=['active_company'])
            return instance.user
        return False


class OrganizationSerializer(serializers.ModelSerializer):
    """All info of company"""

    class Meta:
        model = Organization
        exclude = ('id',)


class TodoSerializer(serializers.ModelSerializer):
    """Fields of 1 Todo in List"""

    leader = serializers.SlugRelatedField(read_only=True,
                                          slug_field='full_name')
    participants = serializers.SlugRelatedField(read_only=True,
                                                slug_field='full_name',
                                                many=True)

    class Meta:
        model = Todo
        fields = ('name', 'commentary', 'leader', 'participants')


class TodoListSerializer(serializers.Serializer):
    """Separating organization from Todo"""

    organization = OrganizationSerializer()
    todo_list = TodoSerializer(many=True)

