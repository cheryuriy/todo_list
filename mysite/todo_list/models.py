from django.contrib.auth.models import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _


class CustomUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        if not password:
            raise ValueError('The password must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email


class Organization(models.Model):
    name = models.CharField(max_length=255, unique=True)
    info = models.TextField(max_length=1000, null=True)
    contacts = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class Profile(models.Model):
    """User's profile"""

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    active_company = models.ForeignKey(Organization,
                                       on_delete=models.SET_NULL,
                                       null=True,
                                       related_name='active_company')
    organizations = models.ManyToManyField(Organization)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return "".join([self.first_name, " ", self.last_name])

    @property
    def full_name(self):
        return self.__str__()


class Todo(models.Model):
    name = models.CharField(max_length=255)
    commentary = models.TextField(max_length=3000, null=True)
    leader = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True,
                               related_name='leader')
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    participants = models.ManyToManyField(Profile)

    def __str__(self):
        return self.name
