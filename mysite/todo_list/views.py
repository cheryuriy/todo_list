from .serializers import *
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.contrib.auth import authenticate, login, logout
from rest_framework import status
from django.shortcuts import redirect
from rest_framework.permissions import IsAuthenticated, AllowAny


@api_view(['GET'])
def log_out(request):
    logout(request)
    return redirect('/')


@api_view(['GET'])
def api_root(request, format=None):
    """DRF API in root url.

        2 types of API will be rendered:
        1) For not loged in.
        2) For loged users.
    """

    user = request.user
    if not user.is_authenticated:
        urls = {
            'registration': reverse('registration', request=request,
                                    format=format),
            'login': reverse('login', request=request, format=format),
        }
        return Response(urls)
    urls = {
        'logout': reverse('logout', request=request, format=format),
        'todo_list': reverse('todo_list', request=request, format=format),
    }
    return Response(urls)


class Registration(generics.CreateAPIView):
    """Anyone can register."""

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        self.create(request, *args, **kwargs)
        return redirect(reverse('login', request=request))


class Login(generics.UpdateAPIView):
    """Choose company for access it's To_do List."""

    queryset = Profile.objects.all()
    serializer_class = LoginSerializer
    permission_classes = [AllowAny]

    def put(self, request, *args, **kwargs):
        """Custom Login"""

        if request.data.get("email") and request.data.get("password"):
            email = request.data["email"]
            password = request.data['password']
            user = authenticate(request, username=email, password=password)
            if not user:
                return Response("Wrong credentials.",
                                status=status.HTTP_404_NOT_FOUND)
            serializer = self.serializer_class(user.profile, data=request.data)
            if serializer.is_valid():
                user = serializer.save()
                if user:
                    login(request, user)
                    urls = {
                        'logout': reverse('logout', request=request),
                        'todo_list': reverse('todo_list', request=request)
                    }
                    return Response(urls)
                if user is False:
                    return Response("Wrong company.",
                                    status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response("Email and password must be set.",
                        status=status.HTTP_400_BAD_REQUEST)


class TodoList(generics.ListAPIView):
    """Representing organization and it's ToDo List"""

    queryset = Todo.objects.all()
    serializer_class = TodoListSerializer
    permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        user = self.request.user
        active_company = user.profile.active_company
        queryset = queryset.filter(organization=active_company)
        data = {"todo_list": queryset,
                "organization": active_company}
        serializer = self.get_serializer(data)
        return Response(serializer.data)

