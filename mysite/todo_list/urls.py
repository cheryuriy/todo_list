from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *


urlpatterns = format_suffix_patterns([
    url(r'^$', api_root),
    url(r'^registration/$', Registration.as_view(), name='registration'),
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', log_out, name='logout'),
    url(r'^todo_list/', TodoList.as_view(), name='todo_list'),
])
